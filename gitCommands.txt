touch file.txt

git --version 
Shows the git version installed 

git init 
Creates a new local repository 

git add file.txt 
git add .
Adds file in the repository

git status 
List changes that you have made in your project everything will be red/green
	
git commit -m "initial commit"
Commit changes in the repository with an added message 

git log 
stores changes history of all version

git remote add Test "https://bitbucket.org/heman-7/test.git"
creates connection with remote repo

git push --force Test master
push changes done in local repo to the remote repo

For cases when push command does'nt works 
git config --global http.sslVerify false
(SSL certificate) 

Creating a new branch name beta and merging it with master branch
git branch <branch_name>
git branch beta 
creates a new branch name beta

git checkout <branch_name>
git checkout beta 
make beta as a current branch

git merge <branch_name>
git merge master
merge master branch with current beta branch

git push origin beta
push the changes to the remote repo.
